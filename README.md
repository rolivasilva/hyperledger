# Configuracion de Hyperledger con tu propio dominio

Configurar Hyperledger (one peer)

---
## Summary

- [Prerequisites](#prerequisites)
- [Installing components](#installing-components)
- [Create Fabric Business Network](#create-hyperledger-fabric-business-network)
- [Generate Certificates, composer genesis an composer channel](#generate-certificates-composer-genesis-an-composer-channel)
- [Change docker compose file](#Change-docker-compose-file)
- [Replace files for intialization of container](#replace-files-for-intialization-of-container)
---

### Prerequisites

To run `bashHyperledger Composer` and `Hyperledger Fabric`, I recommend you have at least 4Gb of memory and 20 GB of HdDisk

The following are prerequisites for installing the required development tools:

- Operating Systems: Ubuntu Linux 16.04 64-bit.
- Docker Engine: Version 17.12.1 or higher
- Docker-Compose: Version 1.13 o
- Node: 8.10.0 or higher (version 9 is not supported)
- npm: v5.7.1
- git: 2.9.x or higher
- Python: 2.7.12


Before to install  the prerequisites, we have to download the project from bitbucker
```bash
cd ~ && git clone https://rolivasilva@bitbucket.org/rolivasilva/hyperledger.git
```

If you have an installation is clean, you must install the prerequisites
```bash
cd ~/hyperledger/config && ./prereqsUbuntu.sh
```

**Please, remember to reload the enviroment variable**
---

### Installing components

There are a few useful CLI tools to used. The most important one is `composer-cli`, which contains all the essential operations, also pick up `generator-hyperledger-composer`, `composer-rest-server` and `Yeoman` plus the generator-hyperledger-composer.

- Installing the Utilities: Essential CLI tools, REST Server, generating application assets, Yeoman and "Playground" UI  :
```bash
npm install -g composer-cli
npm install -g composer-rest-server
npm install -g generator-hyperledger-composer
npm install -g yo
npm install -g composer-playground
```

**Move the folder to the user's home, if you prefer not to move it, remember to change the route of the scripts that are run in the next steps**
---

### Create Hyperledger Fabric Business Network

Edit `crypto-config.yaml` and `configtx.yaml` files and replace the example.com domain to the domain of your prefereference (ex. plataformait.com, This will be the domain used throughout the guide)

- Open crypto-config.yaml and replace 'example.com'

```bash
cd ~/hyperledger/fabric-scripts/hlfv1/composer
pico crypto-config.yaml
```

- Open pico configtx.yaml and replace 'example.com'

```bash
pico configtx.yaml
```
---

### Generate Certificates, composer genesis an composer channel

 Run the next command for generate the folder `ordererOrganitations` and `peerOrganizations` within `crypto-config`


- Go to composer folder, here is the config files for create Business Network, export env `FABRIC_CFG_PATH` and Generate the certificates

```bash
cd ~/hyperledger/fabric-scripts/hlfv1/composer
~/hyperledger/bin/cryptogen generate --config=./crypto-config.yaml
```

- Create `composer-genesis.block` file

```bash
export FABRIC_CFG_PATH=$PWD
~/hyperledger/bin/configtxgen -profile ComposerOrdererGenesis -outputBlock ./composer-genesis.block
```

- Create composer-`composer-channel.tx` file with channelID `composerchannel`

```bash
~/hyperledger/bin/configtxgen -profile ComposerChannel -outputCreateChannelTx ./composer-channel.tx -channelID composerchannel
```
---

### Change docker compose file

Open the docker-compose replace the domain example.com to your domain prefer (ex. plataformait.com)

- Change to composer folder
```bash
cd ~/hyperledger/fabric-scripts/hlfv1/composer
```

- Search the ca privatekey on peerOrganizations file `_sk`
```bash
ls ./crypto-config/peerOrganizations/org1.plataformait.com/ca/
addfcea985c5c60a482c5ab8ef40bcb5a3c0961c35aac5fdf9f1bb8fb581d5bf_sk  ca.org1.plataformait.com-cert.pem
```

- Copy the name of the file `sk` and open the `docker-compose.yml` file
```bash
pico docker-compose.yml
```   

- Replace the line `command:` on the container `ca.org1.plataformait.com` and replace example.com

**old:**
```
command: sh -c 'fabric-ca-server start --ca.certfile /etc/hyperledger/fabric-ca-server-config/ca.org1.plataformait.com-cert.pem --ca.keyfile /etc/hyperledger/fabric-ca-server-config/19ab65abbb04807dad12e4c0a9aaa6649e70868e3abd0217a322d89e47e1a6ae_sk -b admin:adminpw -d'
```

**new:**
```
 command: sh -c 'fabric-ca-server start --ca.certfile /etc/hyperledger/fabric-ca-server-config/ca.org1.plataformait.com-cert.pem --ca.keyfile /etc/hyperledger/fabric-ca-server-config/addfcea985c5c60a482c5ab8ef40bcb5a3c0961c35aac5fdf9f1bb8fb581d5bf_sk -b admin:adminpw -d'
```
---

### Replace files for intialization of container

Edit the `startFabric.sh`, `createComposerProfile.sh` and `createPeerAdminCard.sh` files and replace the domain, example.com to your domain prefer (ex. plataformait.com) and search the ca privatekey on `createPeerAdminCard.sh`

- Replace example.com on the `startFabric.sh`, `createComposerProfile.sh` files
```bash
cd ~/hyperledger/fabric-scripts/hlfv1
pico startFabric.sh
pico createComposerProfile.sh
```

- Search the ca privatekey on peerOrganizations file `_sk`

```bash
ls composer/crypto-config/peerOrganizations/org1.plataformait.com/users/Admin@org1.plataformait.com/msp/keystore/
37bdf54340f362b640673a70178946b1dd39dc2bd6bd29dd01fb1ec0287cdfbd_sk
```

- Copy the name of the file `sk` and open the `createPeerAdminCard.sh` file  
```bash
pico createPeerAdminCard.sh
```

- Replace the line `PRIVATE_KEY` and remember to replace example.com again

**old:**
```
 PRIVATE_KEY="${DIR}"/composer/crypto-config/peerOrganizations/org1.plataformait.com/users/Admin@org1.plataformait.com/msp/keystore/114aab0e76bf0c78308f89efc4b8c9423e31568da0c340ca187a9b17aa9a4457_sk
```

**new:**
```
 PRIVATE_KEY="${DIR}"/composer/crypto-config/peerOrganizations/org1.plataformait.com/users/Admin@org1.plataformait.com/msp/keystore/37bdf54340f362b640673a70178946b1dd39dc2bd6bd29dd01fb1ec0287cdfbd_sk
```
---

### Initializing fabric

Initializing fabric and deploy the containers

```bash
cd ~/hyperledger
./startFabric.sh
./createPeerAdminCard.sh
```

### Start the web app ("Playground") and background

this is only some way of run in background, it run by port 8080
```bash
screen -d -m composer-playground
```
### Creating a business network structure


```bash
yo hyperledger-composer:businessnetwork
```
Enter it-network for the network name,
and desired information for description, author name, and author email.
Select Apache-2.0 as the license.
Select plataformait.com as the namespace.


Generate a business network archive
```bash
cd it-networks
composer archive create -t dir -n .
```
Deploying the business network
```bash
composer runtime install --card PeerAdmin@hlfv1 --businessNetworkName it-network
```
The composer network start command requires a business network card

```bash
composer network start --card PeerAdmin@hlfv1 --networkAdmin admin --networkAdminEnrollSecret adminpw --archiveFile it-network@0.0.1.bna --file networkadmin.card
```

To import the network administrator identity as a usable business network card

```bash
composer card import --file networkadmin.card
```

*To check that the business network has been deployed successfully, run the following command to ping the network*

```bash
composer network ping --card admin@it-network
```

Generating a REST server, port 3000

```bash
composer-rest-server --card  admin@it-network --namespaces never
```
